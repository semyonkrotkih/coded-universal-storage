﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Coded.Universal.Storage.Configuration;
using Coded.Universal.Storage.DB.Model;
using Coded.Universal.Storage.DB.Provider;
using Coded.Universal.Storage.DB.Provider.Interface;
using Coded.Universal.Storage.JsonFormatter;
using Coded.Universal.Storage.Services;
using Coded.Universal.Storage.Services.Interface;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authentication.OAuth;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Swashbuckle.AspNetCore.Swagger;

namespace Coded.Universal.Storage
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // Context accessor
            var httpContextAccessor = new HttpContextAccessor();
            services.AddSingleton<IHttpContextAccessor>(httpContextAccessor);

            services.AddSingleton<IFileService, FileService>();
            services.AddSingleton<ICloudService, CloudService>();
            services.AddSingleton<IImageService, ImageService>();

            // Database providers
            services.AddSingleton<IFileProvider, FileProvider>();
            services.AddSingleton<IUserProvider, UserProvider>();
            services.AddSingleton<IElasticsearchProvider, ElasticsearchProvider>();
            services.AddSingleton<ITextProvider, TextProvider>();
            services.AddSingleton<ITagProvider, TagProvider>();

            services.Configure<AppSettings>(Configuration.GetSection("AppSettings"));
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info { Title = "Universal Storage", Version = "v1" });
            });

            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(options =>
                {
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuer = true,
                        ValidateAudience = true,
                        ValidateLifetime = true,
                        ValidateIssuerSigningKey = true,
                        ValidIssuer = Configuration["Jwt:Issuer"],
                        ValidAudience = Configuration["Jwt:Audience"],
                        IssuerSigningKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(Configuration["Jwt:Secret"]))
                    };
                });

            services.AddCors();

            services.AddMvc()
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_1)
                .AddJsonOptions(options =>
                {
                    options.SerializerSettings.Formatting = Formatting.Indented;
                    options.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
                    options.SerializerSettings.DefaultValueHandling = DefaultValueHandling.Include;
                    options.SerializerSettings.NullValueHandling = NullValueHandling.Ignore;
                    options.SerializerSettings.Converters.Add(new DateTimeJavascriptConverter(httpContextAccessor));
                });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, IFileProvider fileProvider)
        {
            //if (env.IsDevelopment())
            //{
                app.UseDeveloperExceptionPage();
            //}
            //else
            //{
            //    app.UseHsts();
            //}

            //fileProvider.CreateIndexAsync().Wait();

            app.UseHttpsRedirection();
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Universal Storage V1");
            });
            var supportedCultures = new[] { new CultureInfo("en-GB"), new CultureInfo("da-DK"), new CultureInfo("se-SE") };
            app.UseRequestLocalization(new RequestLocalizationOptions
            {
                DefaultRequestCulture = new Microsoft.AspNetCore.Localization.RequestCulture("en-GB"),
                SupportedCultures = supportedCultures
            });
            app.UseAuthentication();
            app.UseCors(builder =>
                builder.WithOrigins("http://localhost:88", "http://testeseebase.azurewebsites.net", "https://testeseebase.azurewebsites.net", "https://eseebase.azurewebsites.net", "https://portal.eseebase.com")
                .AllowAnyHeader()
                .AllowAnyMethod()
                .AllowCredentials());
            app.UseMvc();
        }
    }
}
