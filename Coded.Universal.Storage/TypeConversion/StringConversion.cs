﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Coded.Universal.Storage.TypeConversion
{
    public static class StringConversion
    {
        public static int? ParseAsNullableInteger(this string input)
        {
            if (string.IsNullOrEmpty(input))
            {
                return (int?)null;
            }
            int result;
            if (int.TryParse(input, out result))
            {
                return (int?)result;
            }
            return null;
        }
    }
}
