﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Coded.Universal.Storage.Services.Interface
{
    public interface ICloudService
    {
        Task<string> StoreFileAsync(byte[] data, string filename, string contentType, ObjectId id);
        Task DeleteAsync(Uri uri);
        Task DeleteAllAsync();
    }
}
