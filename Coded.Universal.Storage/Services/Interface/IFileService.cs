﻿using Coded.Universal.Storage.DB.Model;
using Coded.Universal.Storage.ViewModel;
using Microsoft.AspNetCore.Http;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace Coded.Universal.Storage.Services.Interface
{
    public interface IFileService
    {
        Task<List<File>> UploadAsync(HttpRequest request, string companyNumber, string associationId, string departmentId, string localizer, string associationNumber, string departmentNumber, string userName);
        Task<PagedResult<File>> ScrollAsync(string scrollId);
        Task<PagedResult<File>> SearchAsync(string companyNumber, ResultFilter filter);
        Task DeleteAsync(ObjectId id);
        Task UpdateTagsAsync(ObjectId id, List<string> tags);
        Task ProduceTestDataAsync(string category);
        Task DeleteAllAsync();
        Task UpdateDescriptionAsync(ObjectId id, string description);
        Task CreateIndexesAsync();
        Task<bool> AnyAsync(string companyNumber, string associationId, string departmentId, string localizer);
        Task<List<File>> AddLinkAsync(string companyNumber, FilePropertyModel model, string userName);
        Task UpdateIssueDateAsync(ObjectId id, DateTime? issueDate);
    }
}
