﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Coded.Universal.Storage.Services.Interface
{
    public interface IImageService
    {
        byte[] ResizeImage(string url, int newSize);
        bool IsUrl(string url);
    }
}
