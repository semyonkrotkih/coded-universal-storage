﻿using Coded.Universal.Storage.Configuration;
using Coded.Universal.Storage.CustomException;
using Coded.Universal.Storage.ViewModel;
using Coded.Universal.Storage.DB.Provider;
using Coded.Universal.Storage.Services.Interface;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using Coded.Universal.Storage.DB.Provider.Interface;
using Coded.Universal.Storage.DB.Model;
using MongoDB.Bson;
using System.Drawing;

namespace Coded.Universal.Storage.Services
{
    public class FileService: IFileService
    {
        private IHostingEnvironment _hostingEnvironment;
        private AppSettings _appSettings;
        private ICloudService _cloudService;
        private IFileProvider _fileProvider;
        private IImageService _imageProvider;
        public FileService(IHostingEnvironment environment, IOptions<AppSettings> settings, ICloudService cloudService, IFileProvider fileProvider, IImageService imageProvider)
        {
            _hostingEnvironment = environment;
            _appSettings = settings.Value;
            _cloudService = cloudService;
            _fileProvider = fileProvider;
            _imageProvider = imageProvider;
        }

        public async Task<List<File>> UploadAsync(HttpRequest request, string companyNumber, string associationId, string departmentId, string localizer, string associationNumber, string departmentNumber, string userName)
        {
            if (!request.HasFormContentType || request.Form == null)
            {
                throw new CodedUnsupportedMediaTypeException();
            }

            associationNumber = associationNumber.PadLeft(4, '0');
            departmentNumber = departmentNumber.PadLeft(4, '0');
            var result = new List<File>();
            foreach (var file in request.Form.Files)
            {
                var fileName = HttpUtility.UrlDecode(file.FileName.ToLowerInvariant()).Replace(@"""", "");
                var fileDb = await _fileProvider.CreateAsync(new DB.Model.File()
                {
                    FileName = fileName,
                    CompanyNumber = companyNumber,
                    AssociationId = associationId,
                    DepartmentId = departmentId,
                    Localizers = !string.IsNullOrEmpty(localizer) ? localizer.Split(",").ToList() : null,
                    Tags = new List<string>(),
                    AssociationNumber = associationNumber,
                    DepartmentNumber = departmentNumber,
                    Timestamp = DateTime.UtcNow,
                    UserName = userName
                });
                using (var stream = new System.IO.MemoryStream())
                using (var imageStream = new System.IO.MemoryStream())
                {
                    stream.Position = 0;
                    await file.CopyToAsync(stream);
                    var originalImage = Image.FromStream(stream);

                    if (originalImage.PropertyIdList.Contains(0x0112))
                    {
                        var rotationValue = originalImage.GetPropertyItem(0x0112).Value[0];
                        switch (rotationValue)
                        {
                            case 1: // landscape, do nothing
                                break;

                            case 8: // rotated 90 right
                                    // de-rotate:
                                originalImage.RotateFlip(rotateFlipType: RotateFlipType.Rotate270FlipNone);
                                break;

                            case 3: // bottoms up
                                originalImage.RotateFlip(rotateFlipType: RotateFlipType.Rotate180FlipNone);
                                break;

                            case 6: // rotated 90 left
                                originalImage.RotateFlip(rotateFlipType: RotateFlipType.Rotate90FlipNone);
                                break;
                        }
                    }
                    imageStream.Seek(0, System.IO.SeekOrigin.Begin);
                    originalImage.Save(imageStream, System.Drawing.Imaging.ImageFormat.Png);
                    imageStream.Seek(0, System.IO.SeekOrigin.Begin);
                    imageStream.Close();

                    fileDb.Url = await _cloudService.StoreFileAsync(imageStream.ToArray(), fileName, file.Headers["Content-Type"], fileDb.Id);
                    await _fileProvider.UpdateUrlAsync(fileDb.Id, fileDb.Url);
                }
                await _fileProvider.PutIntoIndexAsync(fileDb);
                result.Add(fileDb);
            }
            return result;
        }

        public async Task<List<File>> AddLinkAsync(string companyNumber, FilePropertyModel model, string userName)
        {
            if (!_imageProvider.IsUrl(model.Link))
            {
                throw new ArgumentException("Invalid URL string");
            }
            var associationNumber = model.AssociationNumber.PadLeft(4, '0');
            var departmentNumber = model.DepartmentNumber.PadLeft(4, '0');
            var result = new List<File>();
            if (!string.IsNullOrEmpty(model.Link))
            {
                var fileName = string.Empty;
                Uri uri = new Uri(model.Link);
                if (uri.IsFile || (uri.Segments.Length > 0 && !string.IsNullOrEmpty(System.IO.Path.GetExtension(uri.Segments[uri.Segments.Length - 1]))))
                {
                    fileName = HttpUtility.UrlDecode(System.IO.Path.GetFileName(uri.AbsolutePath)).Replace(@"""", "");
                }
                var fileDb = await _fileProvider.CreateAsync(new DB.Model.File()
                {
                    FileName = fileName,
                    CompanyNumber = companyNumber,
                    AssociationId = model.AssociationId,
                    DepartmentId = model.DepartmentId,
                    Localizers = model.Localizers,
                    Tags = new List<string>(),
                    AssociationNumber = associationNumber,
                    DepartmentNumber = departmentNumber,
                    Url = model.Link,
                    Timestamp = DateTime.UtcNow,
                    UserName = userName
                });
                await _fileProvider.PutIntoIndexAsync(fileDb);
                result.Add(fileDb);
            }
            return result;
        }

        public async Task<PagedResult<File>> SearchAsync(string companyNumber, ResultFilter filter)
        {
            return await _fileProvider.SearchAsync(companyNumber, filter);
        }

        public async Task<PagedResult<File>> ScrollAsync(string scrollId)
        {
            return await _fileProvider.ScrollAsync(scrollId);
        }

        public async Task DeleteAsync(ObjectId id)
        {
            var file = await _fileProvider.GetAsync(id);
            if (file != null)
            {
                await _cloudService.DeleteAsync(new Uri(file.Url));
                await _fileProvider.DeleteAsync(id);
            }
        }

        public async Task UpdateTagsAsync(ObjectId id, List<string> tags)
        {
            await _fileProvider.UpdateTagsAsync(id, tags);
        }

        public async Task ProduceTestDataAsync(string category)
        {
            await _fileProvider.ProduceTestDataAsync(category);
        }

        public async Task UpdateDescriptionAsync(ObjectId id, string description)
        {
            await _fileProvider.UpdateDescriptionAsync(id, description);
        }

        public async Task UpdateIssueDateAsync(ObjectId id, DateTime? issueDate)
        {
            await _fileProvider.UpdateIssueDateAsync(id, issueDate);
        }

        public async Task DeleteAllAsync()
        {
            await _fileProvider.DeleteAllAsync();
            await _cloudService.DeleteAllAsync();
        }

        public async Task CreateIndexesAsync()
        {
            await _fileProvider.CreateIndexAsync();
        }

        public async Task<bool> AnyAsync(string companyNumber, string associationId, string departmentId, string localizer)
        {
            var filter = new ResultFilter();
            filter.AssociationIds = new[] { associationId };
            filter.DepartmentIds = new[] { departmentId };
            filter.Localizers = new[] { localizer };
            var searchResult = await _fileProvider.SearchAsync(companyNumber, filter);
            return searchResult.TotalNumberOfRecords > 0;
        }

        private string MapPath(string path)
        {
            var filePath = System.IO.Path.Combine(_hostingEnvironment.WebRootPath, path);
            return filePath;
        }
    }
}
