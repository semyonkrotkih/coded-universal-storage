﻿using Coded.Universal.Storage.Configuration;
using Coded.Universal.Storage.Services.Interface;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Options;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Auth;
using Microsoft.WindowsAzure.Storage.Blob;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Coded.Universal.Storage.Services
{
    public class CloudService: ICloudService
    {
        private AppSettings _appSettings;
        private CloudBlobContainer _container;
        public CloudService(IOptions<AppSettings> settings)
        {
            _appSettings = settings.Value;
            var creds = new StorageCredentials(_appSettings.StorageAccount, _appSettings.StorageKey);
            var storageAccount = CloudStorageAccount.Parse(_appSettings.AzureConnectionString);
            var blobClient = storageAccount.CreateCloudBlobClient();
            _container = blobClient.GetContainerReference("universalstorage");
            _container.CreateIfNotExistsAsync().Wait();
            _container.SetPermissionsAsync(
                new BlobContainerPermissions { PublicAccess = BlobContainerPublicAccessType.Blob }).Wait();
        }

        public async Task<string> StoreFileAsync(byte[] data, string filename, string contentType, ObjectId id)
        {
            var blob = _container.GetBlockBlobReference(string.Concat(id.ToString(), "/", filename));
            if (await blob.ExistsAsync())
            {
                await blob.CreateSnapshotAsync();
            }
            await blob.UploadFromByteArrayAsync(data, 0, data.Length);

            blob.Properties.ContentType = contentType;
            await blob.SetPropertiesAsync();

            return blob.Uri.ToString();
        }

        public async Task DeleteAsync(Uri uri)
        {
            var blob = _container.GetBlobReference(uri.AbsolutePath.Substring(1));
            await blob.DeleteIfExistsAsync(DeleteSnapshotsOption.IncludeSnapshots, null, null, null);
        }

        public async Task DeleteAllAsync()
        {
            await _container.ListBlobsSegmentedAsync(null).ContinueWith((Task<BlobResultSegment> t) =>
            {
                if (t.IsCanceled || t.IsFaulted)
                {
                    throw new Exception("Cannot get a list of blobs: " + t.Exception != null ? t.Exception.Message : "");
                }
                var result = t.Result;
                var results = result.Results;
                foreach (CloudBlob blob in results)
                {
                    blob.DeleteIfExistsAsync(DeleteSnapshotsOption.IncludeSnapshots, null, null, null).Wait();
                }
            });
        }
    }
}
