﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Drawing;
using Coded.Universal.Storage.Services.Interface;
using System.Net;
using System.IO;
using System.Text.RegularExpressions;

namespace Coded.Universal.Storage.Services
{
    public class ImageService: IImageService
    {
        public byte[] ResizeImage(string url, int newSize)
        {
            if (!IsUrl(url))
            {
                throw new ArgumentException("Invalid URL string");
            }
            using (var webClient = new WebClient())
            using (var imageStream = new MemoryStream(webClient.DownloadData(url)))
            {
                var image = new Bitmap(imageStream);
                var resized = Resize(newSize, image);
                using (var ms = new MemoryStream())
                {
                    resized.Save(ms, image.RawFormat);
                    return ms.ToArray();
                }
            }
        }

        public bool IsUrl(string url)
        {
            return Regex.Match(url, @"(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?").Success;
        }

        private static Image Resize(int newSize, Image originalImage)
        {
            if (originalImage.Width <= newSize)
                newSize = originalImage.Width;

            var newHeight = originalImage.Height * newSize / originalImage.Width;

            if (newHeight > newSize)
            {
                // Resize with height instead
                newSize = originalImage.Width * newSize / originalImage.Height;
                newHeight = newSize;
            }

            return originalImage.GetThumbnailImage(newSize, newHeight, null, IntPtr.Zero);
        }
    }
}
