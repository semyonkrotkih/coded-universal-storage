﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Coded.Universal.Storage.ViewModel
{
    public class FilePropertyModel
    {
        public string AssociationId { get; set; }
        public string DepartmentId { get; set; }
        public List<string> Localizers { get; set; }
        public string AssociationNumber { get; set; }
        public string DepartmentNumber { get; set; }
        public string Link { get; set; }
    }
}
