﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Coded.Universal.Storage.ViewModel
{
    public class DescriptionWrapperViewModel
    {
        public string Description { get; set; }
    }

    public class DateWrapperViewModel
    {
        public DateTime? Date { get; set; }
    }
}
