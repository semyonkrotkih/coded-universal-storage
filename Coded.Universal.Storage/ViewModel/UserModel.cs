﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Coded.Universal.Storage.ViewModel
{
    public class UserModel
    {
        public string Name { get; set; }
        public string Email { get; set; }
    }
}
