﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Coded.Universal.Storage.ViewModel
{
    public class FileModel
    {
        public string Category { get; set; }
        public List<string> Tags { get; set; }
        public string Url { get; set; }
        public string FileName { get; set; }
    }
}
