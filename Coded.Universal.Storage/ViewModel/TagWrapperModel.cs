﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Coded.Universal.Storage.ViewModel
{
    public class TagWrapperModel
    {
        public List<string> Tags { get; set; }
    }
}
