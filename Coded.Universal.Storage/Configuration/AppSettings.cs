﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Coded.Universal.Storage.Configuration
{
    public class AppSettings
    {
        public string UploadFolder { get; set; }

        public string StorageLocation { get; set; }

        public string StorageAccount { get; set; }

        public string StorageKey { get; set; }

        public string AzureConnectionString { get; set; }
    }
}
