﻿using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Coded.Universal.Storage.TypeConversion;

namespace Coded.Universal.Storage.JsonFormatter
{
    public class DateTimeJavascriptConverter : IsoDateTimeConverter
    {
        private IHttpContextAccessor _httpContextAccessor;
        public DateTimeJavascriptConverter(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }

        public override bool CanConvert(Type objectType)
        {
            return (objectType == typeof(DateTime) || objectType == typeof(DateTime?));
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            var baseResult = base.ReadJson(reader, objectType, existingValue, serializer);
            var date = baseResult as DateTimeOffset?;

            if (date.HasValue)
            {
                //timezone offset
                var timezoneOffsetMinutes = _httpContextAccessor.HttpContext.Request.Headers["TimezoneOffsetMinutes"].FirstOrDefault().ParseAsNullableInteger();
                var timezoneOffset = timezoneOffsetMinutes.HasValue
                    ? TimeSpan.FromMinutes(timezoneOffsetMinutes.Value)
                    : (TimeSpan?)null;

                return timezoneOffset.HasValue
                    ? date.Value.ToOffset(timezoneOffset.Value)
                    : date.Value;
            }

            return baseResult;
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            if (value != null && (value is DateTime || value is DateTime?))
            {
                var d = (DateTime?)value;
                if (d.HasValue) // If value was a DateTime?, then this is possible
                {
                    var timezoneOffsetMinutes = _httpContextAccessor.HttpContext.Request.Headers["TimezoneOffsetMinutes"].FirstOrDefault().ParseAsNullableInteger();
                    var dateWithOffset = timezoneOffsetMinutes.HasValue ? d.Value.AddMinutes(-1 * timezoneOffsetMinutes.Value) : d.Value;
                    writer.WriteRawValue("\"" + dateWithOffset.ToString("dd-MM-yyyy HH:mm") + "\"");
                    return;
                }
            }
            writer.WriteRawValue(JsonConvert.ToString(value));
        }
    }
}
