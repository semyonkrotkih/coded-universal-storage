﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Coded.Universal.Storage.DB.Provider.Interface;
using Coded.Universal.Storage.ViewModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;

namespace Coded.Universal.Storage.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TokenController : ControllerBase
    {
        private IConfiguration _configuration;
        private IUserProvider _userProvider;
        public TokenController(IUserProvider userProvider, IConfiguration configuration)
        {
            _userProvider = userProvider;
            _configuration = configuration;
        }

        //[AllowAnonymous]
        //[HttpPost]
        //public async Task<IActionResult> CreateToken([FromBody]LoginModel login)
        //{
        //    IActionResult response = Unauthorized();
        //    var user = await AuthenticateAsync(login);

        //    if (user != null)
        //    {
        //        var tokenString = BuildToken(user);
        //        response = Ok(new { token = tokenString });
        //    }

        //    return response;
        //}

        private async Task<UserModel> AuthenticateAsync(LoginModel login)
        {
            var user = await _userProvider.ValidateUserAsync(login.Username, login.Password);
            if (user != null)
            {
                return new UserModel()
                {
                    Name = user.Name,
                    Email = user.Email
                };
            }
            return null;
        }

        private string BuildToken(UserModel user)
        {

            var claims = new[] {
                new Claim(JwtRegisteredClaimNames.Sub, user.Name),
                new Claim(JwtRegisteredClaimNames.Email, user.Email),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString())
            };

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["Jwt:Secret"]));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

            var token = new JwtSecurityToken(_configuration["Jwt:Issuer"],
              _configuration["Jwt:Issuer"],
              claims,
              expires: DateTime.Now.AddMinutes(30),
              signingCredentials: creds);

            return new JwtSecurityTokenHandler().WriteToken(token);
        }
    }
}