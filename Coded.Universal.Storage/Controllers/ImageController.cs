﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Coded.Universal.Storage.Services.Interface;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Coded.Universal.Storage.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ImageController : ControllerBase
    {
        private IImageService _imageService;
        public ImageController(IImageService imageService)
        {
            _imageService = imageService;
        }

        [HttpGet("resize")]
        [ResponseCache(Duration = int.MaxValue)]
        public IActionResult ResizeAsync(string imageUrl)
        {
            return File(_imageService.ResizeImage(imageUrl, 200), "image/jpeg");
        }

    }
}