﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Coded.Universal.Storage.ViewModel;
using Coded.Universal.Storage.DB.Model;
using Coded.Universal.Storage.DB.Provider;
using Coded.Universal.Storage.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Coded.Universal.Storage.DB.Provider.Interface;
using Coded.Universal.Storage.Services.Interface;
using MongoDB.Bson;

namespace Coded.Universal.Storage.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class TagController : ControllerBase
    {
        private ITagProvider _service;
        public TagController(ITagProvider service)
        {
            _service = service;
        }

        [HttpGet("{companyNumber}/search")]
        public async Task<List<string>> TagsSearchAsync(string companyNumber, string searchText)
        {
            return await _service.SearchAsync(companyNumber, searchText);
        }
    }
}