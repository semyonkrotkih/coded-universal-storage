﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Coded.Universal.Storage.ViewModel;
using Coded.Universal.Storage.DB.Model;
using Coded.Universal.Storage.DB.Provider;
using Coded.Universal.Storage.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Coded.Universal.Storage.DB.Provider.Interface;
using Coded.Universal.Storage.Services.Interface;
using MongoDB.Bson;

namespace Coded.Universal.Storage.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class FileController : ControllerBase
    {
        private IFileService _service;
        public FileController(IFileService service)
        {
            _service = service;
        }

        [HttpPost("{companyNumber}/search")]
        public async Task<PagedResult<File>> SearchAsync(string companyNumber, ResultFilter filter)
        {
            return await _service.SearchAsync(companyNumber, filter);
        }

        [HttpGet("scroll")]
        public async Task<PagedResult<File>> ScrollAsync(string scrollId)
        {
            return await _service.ScrollAsync(scrollId);
        }

        [HttpPost("{companyNumber}/upload")]
        public async Task<List<File>> UploadAsync(string companyNumber, string associationId, string departmentId, string localizer, string associationNumber, string departmentNumber)
        {
            var userName = User.Claims.FirstOrDefault(c => c.Type == System.Security.Claims.ClaimTypes.Name)?.Value;
            return await _service.UploadAsync(Request, companyNumber, associationId, departmentId, localizer, associationNumber, departmentNumber, userName);
        }

        [HttpPost("{companyNumber}/addlink")]
        public async Task<IActionResult> AddLinkAsync(string companyNumber, FilePropertyModel model)
        {
            try
            {
                var userName = User.Claims.FirstOrDefault(c => c.Type == System.Security.Claims.ClaimTypes.Name)?.Value;
                return Ok(await _service.AddLinkAsync(companyNumber, model, userName));
            }
            catch (ArgumentException)
            {
                return BadRequest();
            }
        }

        [HttpGet("{companyNumber}/any")]
        [AllowAnonymous]
        public async Task<bool> AnyAsync(string companyNumber, string associationId, string departmentId, string localizer)
        {
            return await _service.AnyAsync(companyNumber, associationId, departmentId, localizer);
        }

        [HttpPost("{id}/tags")]
        public async Task UpdateTagsAsync(string id, TagWrapperModel tags)
        {
            await _service.UpdateTagsAsync(ObjectId.Parse(id), tags.Tags);
        }

        [HttpPost("{category}/generate")]
        public async Task ProduceTestDataAsync(string category)
        {
            await _service.ProduceTestDataAsync(category);
        }

        [HttpPut("{id}/description")]
        public async Task UpdateDescriptionAsync(string id, [FromBody]DescriptionWrapperViewModel description)
        {
            await _service.UpdateDescriptionAsync(ObjectId.Parse(id), description.Description);
        }

        [HttpPut("{id}/issueDate")]
        public async Task UpdateIssueDateAsync(string id, [FromBody]DateWrapperViewModel issueDate)
        {
            await _service.UpdateIssueDateAsync(ObjectId.Parse(id), issueDate.Date);
        }

        [HttpDelete("{id}")]
        public async Task<bool> DeleteAsync(string id)
        {
            await _service.DeleteAsync(ObjectId.Parse(id));
            return true;
        }

        [HttpDelete]
        public async Task DeleteAllAsync(string id)
        {
            await _service.DeleteAllAsync();
        }
    }
}