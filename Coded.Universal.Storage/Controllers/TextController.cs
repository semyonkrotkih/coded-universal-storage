﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Coded.Universal.Storage.DB.Model;
using Coded.Universal.Storage.DB.Provider.Interface;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Coded.Universal.Storage.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class TextController : ControllerBase
    {
        private ITextProvider _provider;
        public TextController(ITextProvider provider)
        {
            _provider = provider;
        }

        [HttpGet("search")]
        public async Task<IEnumerable<Text>> SearchAsync(string tags)
        {
            return await _provider.SearchAsync(tags.Split(',').ToList());
        }

        [HttpPut()]
        public async Task PutAsync(Text text)
        {
            await _provider.InsertAsync(text);
        }

        [HttpPost("insertmany")]
        public async Task InsertManyAsync(List<Text> textItems)
        {
            await _provider.InsertManyAsync(textItems);
        }

        [HttpDelete()]
        public async Task DeleteAsync(string tags)
        {
            await _provider.DeleteByTagsAsync(tags.Split(',').ToList());
        }

    }
}