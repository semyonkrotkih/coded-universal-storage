﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Coded.Universal.Storage.EF.Model
{
    public class File
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }
        [StringLength(25)]
        public string Category { get; set; }
        [StringLength(255)]
        public string FileName { get; set; }

    }
}
