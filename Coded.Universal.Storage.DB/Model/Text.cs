﻿using Coded.Universal.Storage.DB.Provider.Interface;
using MongoDB.Bson.Serialization.Attributes;
using Nest;
using System;
using System.Collections.Generic;
using System.Text;

namespace Coded.Universal.Storage.DB.Model
{
    [ElasticsearchType(IdProperty = nameof(Id))]
    public class Text: ITaggable
    {
        [BsonId]
        public string Id { get; set; }
        [BsonElement]
        public string Type { get; set; }
        [BsonElement]
        public string Value { get; set; }
        [BsonElement]
        public List<string> Tags { get; set; }
    }
}
