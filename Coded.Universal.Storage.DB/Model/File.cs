﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Nest;
using System;
using System.Collections.Generic;
using System.Text;

namespace Coded.Universal.Storage.DB.Model
{
    [ElasticsearchType(IdProperty = nameof(ElasticId))]
    public class File
    {
        private string _elasticId;
        [Text(Ignore = true)]
        public ObjectId Id { get; set; }
        public string ElasticId
        {
            get
            {
                if (string.IsNullOrEmpty(_elasticId))
                {
                    _elasticId = Id.ToString();
                }
                return _elasticId;
            }
            set
            {
                _elasticId = value;
            }
        }
        [BsonElement]
        [Keyword]
        public string AssociationId { get; set; }
        [BsonElement]
        [Keyword]
        public string DepartmentId { get; set; }
        [BsonElement]
        [Keyword]
        public string AssociationNumber { get; set; }
        [BsonElement]
        [Keyword]
        public string DepartmentNumber { get; set; }
        [BsonElement]
        public List<string> Localizers { get; set; }
        [BsonElement]
        [Keyword]
        public string CompanyNumber { get; set; }
        [BsonElement]
        public string FileName { get; set; }
        [BsonElement]
        public List<string> Tags { get; set; }
        [BsonElement]
        public string Url { get; set; }
        [BsonElement]
        public string Description { get; set; }
        [BsonElement]
        [Date(Format = "dd-MM-yyyy HH:mm")]
        public DateTime? Timestamp { get; set; }
        [BsonElement]
        [Date(Format = "dd-MM-yyyy HH:mm")]
        public DateTime? IssueDate { get; set; }
        [BsonElement]
        public string UserName { get; set; }
    }
}
