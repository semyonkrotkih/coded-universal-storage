﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Coded.Universal.Storage.DB.Model
{
    public class ResultFilter
    {
        public string[] AssociationIds { get; set; }
        public string[] DepartmentIds { get; set; }
        public string[] Localizers { get; set; }
        public string[] Keywords { get; set; }
        public DateTime? CreatedAfter { get; set; }
        public DateTime? CreatedBefore { get; set; }
        public DateTime? IssueDateAfter { get; set; }
        public DateTime? IssueDateBefore { get; set; }
        public ResultFilter()
        {
            AssociationIds = new string[] { };
            DepartmentIds = new string[] { };
            Localizers = new string[] { };
            Keywords = new string[] { };
        }
    }
}
