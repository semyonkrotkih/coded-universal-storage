﻿using Coded.Universal.Storage.DB.Provider.Interface;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Coded.Universal.Storage.DB.Provider
{
    public class TagProvider: ITagProvider
    {
        private IElasticsearchProvider _elasticProvider;
        public TagProvider(IElasticsearchProvider elasticProvider)
        {
            _elasticProvider = elasticProvider;
        }

        public async Task<List<string>> SearchAsync(string companyNumber, string searchText)
        {
            return await _elasticProvider.SearchTagsAsync(companyNumber, searchText);
        }
    }
}
