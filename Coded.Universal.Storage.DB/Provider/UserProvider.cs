﻿using Coded.Universal.Storage.DB.Model;
using Coded.Universal.Storage.DB.Provider.Interface;
using Microsoft.Extensions.Configuration;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Coded.Universal.Storage.DB.Provider
{
    public class UserProvider: IUserProvider
    {
        private IMongoDatabase _mongoDatabase;
        public IMongoCollection<User> Users
        {
            get
            {
                return _mongoDatabase.GetCollection<User>("Users");
            }
        }

        public UserProvider(IConfiguration configuration)
        {
            var client = new MongoClient(configuration["MongoDB:ConnectionString"]);
            _mongoDatabase = client.GetDatabase(configuration["MongoDB:DatabaseName"]);
        }

        public async Task<User> ValidateUserAsync(string username, string password)
        {
            return await Users.Find(x => x.Username == username && x.Password == password).FirstOrDefaultAsync();
        }
    }
}
