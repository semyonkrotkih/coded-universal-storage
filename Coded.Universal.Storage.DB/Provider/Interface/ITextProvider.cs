﻿using Coded.Universal.Storage.DB.Model;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Coded.Universal.Storage.DB.Provider.Interface
{
    public interface ITextProvider
    {
        Task InsertAsync(Text text);
        Task DeleteByTagsAsync(List<string> tags);
        Task<IEnumerable<Text>> SearchAsync(List<string> tags);
        Task InsertManyAsync(List<Text> textItems);
    }
}
