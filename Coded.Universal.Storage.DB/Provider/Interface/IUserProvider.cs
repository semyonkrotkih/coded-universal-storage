﻿using Coded.Universal.Storage.DB.Model;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Coded.Universal.Storage.DB.Provider.Interface
{
    public interface IUserProvider
    {
        Task<User> ValidateUserAsync(string username, string password);
    }
}
