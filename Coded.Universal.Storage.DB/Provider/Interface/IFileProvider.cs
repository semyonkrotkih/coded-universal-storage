﻿using Coded.Universal.Storage.DB.Model;
using Coded.Universal.Storage.ViewModel;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Coded.Universal.Storage.DB.Provider.Interface
{
    public interface IFileProvider
    {
        Task<PagedResult<File>> GetAsync(string category, int pageNumber);
        Task<PagedResult<File>> ScrollAsync(string scrollId);
        Task<File> CreateAsync(File file);
        Task UpdateUrlAsync(ObjectId id, string fileUrl);
        Task DeleteAsync(ObjectId id);
        Task UpdateTagsAsync(ObjectId id, List<string> tags);
        Task<PagedResult<File>> SearchAsync(string companyNumber, ResultFilter filter);
        Task ProduceTestDataAsync(string category);
        Task DeleteAllAsync();
        Task UpdateDescriptionAsync(ObjectId id, string description);
        Task<List<string>> GetCategoriesAsync();
        Task<List<File>> GetAsync();
        Task CreateIndexAsync();
        Task PutIntoIndexAsync(File file);
        Task<File> GetAsync(ObjectId id);
        Task UpdateIssueDateAsync(ObjectId id, DateTime? issueDate);
    }
}
