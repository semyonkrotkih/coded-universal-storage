﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Coded.Universal.Storage.DB.Provider.Interface
{
    public interface ITagProvider
    {
        Task<List<string>> SearchAsync(string companyNumber, string searchText);
    }
}
