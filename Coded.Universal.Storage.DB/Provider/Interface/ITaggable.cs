﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Coded.Universal.Storage.DB.Provider.Interface
{
    public interface ITaggable
    {
        List<string> Tags { get; set; }
    }
}
