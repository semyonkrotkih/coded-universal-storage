﻿using Coded.Universal.Storage.DB.Model;
using Coded.Universal.Storage.ViewModel;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Coded.Universal.Storage.DB.Provider.Interface
{
    public interface IElasticsearchProvider
    {
        Task<PagedResult<File>> SearchAsync(string companyNumber, ResultFilter filter);
        Task UpdateDescriptionAsync(ObjectId id, string description);
        Task UpdateTagsAsync(ObjectId id, List<string> tags);
        Task CreateIndexesAsync(List<File> files);
        Task PutAsync<T>(T item) where T: class;
        Task DeleteFileAsync(ObjectId id);
        Task DeleteAllFilesAsync();
        Task DeleteByTagsAsync<T>(List<string> tags) where T : class, ITaggable;
        Task<IEnumerable<T>> SearchByTagsAsync<T>(List<string> tags) where T : class, ITaggable;
        Task InsertManyAsync<T>(List<T> items) where T : class;
        Task<PagedResult<T>> ScrollAsync<T>(string scrollId) where T : class;
        Task UpdateIssueDateAsync(ObjectId id, DateTime? issueDate);
        Task<List<string>> SearchTagsAsync(string companyNumber, string searchText);
    }
}
