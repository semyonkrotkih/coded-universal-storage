﻿using Coded.Universal.Storage.DB.Model;
using Coded.Universal.Storage.DB.Provider.Interface;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Coded.Universal.Storage.DB.Provider
{
    public class TextProvider: ITextProvider
    {
        private IElasticsearchProvider _elasticProvider;

        public TextProvider(IConfiguration configuration, IElasticsearchProvider elasticProvider)
        {
            _elasticProvider = elasticProvider;
        }

        public async Task InsertAsync(Text text)
        {
            await _elasticProvider.PutAsync(text);
        }

        public async Task InsertManyAsync(List<Text> textItems)
        {
            await _elasticProvider.InsertManyAsync(textItems);
        }

        public async Task DeleteByTagsAsync(List<string> tags)
        {
            await _elasticProvider.DeleteByTagsAsync<Text>(tags);
        }

        public async Task<IEnumerable<Text>> SearchAsync(List<string> tags)
        {
            return await _elasticProvider.SearchByTagsAsync<Text>(tags);
        }
    }
}
