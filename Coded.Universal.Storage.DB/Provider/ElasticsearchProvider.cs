﻿using Coded.Universal.Storage.DB.Model;
using Coded.Universal.Storage.DB.Provider.Interface;
using Coded.Universal.Storage.ViewModel;
using Microsoft.Extensions.Configuration;
using MongoDB.Bson;
using Nest;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Linq;

namespace Coded.Universal.Storage.DB.Provider
{
    public class ElasticsearchProvider: IElasticsearchProvider
    {
        private ElasticClient _client;
        private string _storageName;
        private IConfiguration _configuration;

        private async Task<ElasticClient> GetClientAsync()
        {
            if (_client == null)
            {
                var node = new Uri(_configuration["ElasticSearch:Url"]);
                _storageName = _configuration["ElasticSearch:StorageName"];
                var settings = new ConnectionSettings(node);
                settings.DefaultIndex(_storageName).DisableDirectStreaming();
                _client = new ElasticClient(settings);

                if (!_client.IndexExists(_storageName).Exists)
                {
                    var indexSettings = new IndexSettings();
                    indexSettings.NumberOfReplicas = 1;
                    indexSettings.NumberOfShards = 1;
                    var indexState = new IndexState();
                    indexState.Settings = indexSettings;
                    await _client.CreateIndexAsync(_storageName, c => c.Mappings(ms => ms.Map<File>(f => f.AutoMap(typeof(File))
                                                                                                          .Properties(p => p.Keyword(d => d.Name(n => n.AssociationNumber))
                                                                                                                            .Keyword(d => d.Name(n => n.AssociationId))
                                                                                                                            .Keyword(d => d.Name(n => n.DepartmentId))
                                                                                                                            .Keyword(d => d.Name(n => n.DepartmentNumber))
                                                                                                                            .Text(d => d.Name(n => n.Description).Norms(false))
                                                                                                                            .Text(d => d.Name(n => n.FileName).Norms(false))
                                                                                                                            .Text(d => d.Name(n => n.Tags).Norms(false))))
                                                                                         .Map<Text>(t => t.AutoMap(typeof(Text)))
                                                                                ).InitializeUsing(indexState));
                }
            }
            return _client;
        }
        public ElasticsearchProvider(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public async Task CreateIndexesAsync(List<File> files)
        {
            await DeleteAllFilesAsync();

            foreach (File file in files)
            {
                var result = await GetClientAsync();//.IndexAsync(file, i => i.Index(storageName).Refresh(Elasticsearch.Net.Refresh.True));
                //var id = result.Id;
            }
        }

        public async Task PutAsync<T>(T item) where T: class
        {
            var client = await GetClientAsync();
            await client.IndexAsync<T>(item, i => i.Index(_storageName).Type(GetTypeName<T>()).Refresh(Elasticsearch.Net.Refresh.True));
        }

        public async Task InsertManyAsync<T>(List<T> items) where T : class
        {
            var client = await GetClientAsync();
            await client.IndexManyAsync<T>(items, _storageName);
        }

        public async Task DeleteFileAsync(ObjectId id)
        {
            var client = await GetClientAsync();
            await client.DeleteAsync(DocumentPath<File>.Id(id.ToString()), i => i.Index(_storageName).Type(GetTypeName<File>()).Refresh(Elasticsearch.Net.Refresh.True));
        }

        public async Task DeleteByTagsAsync<T>(List<string> tags) where T: class, ITaggable
        {
            var client = await GetClientAsync();
            await client.DeleteByQueryAsync<T>(mu => mu.Query(m => m
                                                        .Term(mm => mm
                                                        .Field(f => f.Tags)
                                                        .Value(tags))
                                               ).Index(_storageName)
                                                .Type(GetTypeName<T>()));
        }

        public async Task<IEnumerable<T>> SearchByTagsAsync<T>(List<string> tags) where T : class, ITaggable
        {
            var client = await GetClientAsync();
            var result = await client.SearchAsync<T>(mu => mu.Query(m => m
                                                        .Term(mm => mm
                                                        .Field(f => f.Tags)
                                                        .Value(tags))
                                               ).Index(_storageName).Type(GetTypeName<T>()));
            return result.Documents;
        }

        public async Task DeleteAllFilesAsync()
        {
            var client = await GetClientAsync();
            if (client.IndexExists(_storageName).Exists)
            {
                await client.DeleteIndexAsync(_storageName);
            }

            await client.CreateIndexAsync(_storageName, c => c.Mappings(ms => ms.Map<File>(f => f.AutoMap(typeof(File)))));
        }

        public async Task UpdateTagsAsync(ObjectId id, List<string> tags)
        {
            var client = await GetClientAsync();
            await client.UpdateAsync<File, object>(DocumentPath<File>.Id(id.ToString()),
                u => u
                .Doc(new { Tags = tags })
                .RetryOnConflict(3)
                .Index(_storageName)
                .Type(GetTypeName<File>())
                .Refresh(Elasticsearch.Net.Refresh.True)
            );
        }

        public async Task UpdateDescriptionAsync(ObjectId id, string description)
        {
            var client = await GetClientAsync();
            await client.UpdateAsync<File, object>(DocumentPath<File>.Id(id.ToString()),
                u => u
                .Doc(new { Description = description })
                .RetryOnConflict(3)
                .Index(_storageName)
                .Type(GetTypeName<File>())
                .Refresh(Elasticsearch.Net.Refresh.True)
            );
        }

        public async Task UpdateIssueDateAsync(ObjectId id, DateTime? issueDate)
        {
            var client = await GetClientAsync();
            await client.UpdateAsync<File, object>(DocumentPath<File>.Id(id.ToString()),
                u => u
                .Doc(new { IssueDate = issueDate })
                .RetryOnConflict(3)
                .Index(_storageName)
                .Type(GetTypeName<File>())
                .Refresh(Elasticsearch.Net.Refresh.True)
            );
        }

        public async Task<PagedResult<File>> SearchAsync(string companyNumber, ResultFilter filter)
        {
            var pageSize = 10;
            var boolQuery = new BoolQuery();
            var musts = new List<QueryContainer>();
            musts.Add(new TermQuery()
            {
                Field = new Field("companyNumber.keyword"),
                Value = companyNumber
            });
            if (filter.Localizers.Any() && filter.DepartmentIds.Any())
            {
                musts.Add(new TermsQuery()
                {
                    Field = new Field("departmentId.keyword"),
                    Terms = filter.DepartmentIds.Select(d => d.ToLower())
                });
                musts.Add(new TermsQuery()
                {
                    Field = new Field("localizers.keyword"),
                    Terms = filter.Localizers
                });
            }
            else if (filter.DepartmentIds.Any())
            {
                musts.Add(new TermsQuery()
                {
                    Field = new Field("departmentId.keyword"),
                    Terms = filter.DepartmentIds.Select(d => d.ToLower())
                });
            }
            else if (filter.AssociationIds.Any())
            {
                musts.Add(new TermsQuery()
                {
                    Field = new Field("associationId.keyword"),
                    Terms = filter.AssociationIds.Select(d => d.ToLower())
                });
            }
            if (filter.Keywords.Any())
            {
                foreach (var keyword in filter.Keywords)
                {
                    musts.Add(new MultiMatchQuery()
                    {
                        Type = TextQueryType.BestFields,
                        Fields = new Field[] {
                            new Field("tags", 0.6),
                            new Field("description", 0.3),
                            new Field("fileName", 0.1),
                            new Field("userName", 0.1)
                        },
                        Query = keyword
                    });
                }
            }
            if (filter.CreatedAfter.HasValue)
            {
                musts.Add(new ScriptQuery()
                {
                    Source = $"Integer.parseInt(new SimpleDateFormat('yyyyMMdd').format(doc['timestamp'].value.getMillis())) >= {filter.CreatedAfter.Value.ToString("yyyyMMdd")}"
                });
            }
            if (filter.CreatedBefore.HasValue)
            {
                musts.Add(new ScriptQuery()
                {
                    Source = $"Integer.parseInt(new SimpleDateFormat('yyyyMMdd').format(doc['timestamp'].value.getMillis())) <= {filter.CreatedBefore.Value.ToString("yyyyMMdd")}"
                });
            }
            if (filter.CreatedAfter.HasValue || filter.CreatedBefore.HasValue)
            {
                musts.Add(new ExistsQuery()
                {
                    Field = "timestamp"
                });
            }

            if (filter.IssueDateAfter.HasValue)
            {
                musts.Add(new ScriptQuery()
                {
                    Source = $"Integer.parseInt(new SimpleDateFormat('yyyyMMdd').format(doc['issueDate'].value.getMillis())) >= {filter.IssueDateAfter.Value.ToString("yyyyMMdd")}"
                });
            }
            if (filter.IssueDateBefore.HasValue)
            {
                musts.Add(new ScriptQuery()
                {
                    Source = $"Integer.parseInt(new SimpleDateFormat('yyyyMMdd').format(doc['issueDate'].value.getMillis())) <= {filter.IssueDateBefore.Value.ToString("yyyyMMdd")}"
                });
            }
            if (filter.IssueDateAfter.HasValue || filter.IssueDateBefore.HasValue)
            {
                musts.Add(new ExistsQuery()
                {
                    Field = "issueDate"
                });
            }
            boolQuery.Must = musts;
            var client = await GetClientAsync();
            ISearchResponse<File> result = null;
            if (!filter.Keywords.Any())
            {
                result = await client.SearchAsync<File>(fl => fl
                                   .From(0)
                                   .Size(pageSize)
                                   .Index(_storageName)
                                   .Type(GetTypeName<File>())
                                   .Sort(ss => ss.Ascending(new Field("associationNumber.keyword")).Ascending(new Field("departmentNumber.keyword")).Ascending(new Field("localizers.keyword")))
                                   .Query(q => boolQuery)
                                   .Scroll("5m")).ContinueWith(t => {
                                       var x = Encoding.UTF8.GetString(t.Result.ApiCall.RequestBodyInBytes);
                                       return t.Result;
                                   });
            }
            else
            {
                result = await client.SearchAsync<File>(fl => fl
                                .From(0)
                                .Size(pageSize)
                                .Index(_storageName)
                                .Type(GetTypeName<File>())
                                .Sort(ss => ss.Descending(SortSpecialField.Score))
                                .Query(q => boolQuery)
                                .Scroll("5m")).ContinueWith(t => {
                                    var x = Encoding.UTF8.GetString(t.Result.ApiCall.RequestBodyInBytes);
                                    return t.Result;
                                });
            }
            

            return new PagedResult<File>()
            {
                PageSize = pageSize,
                TotalNumberOfRecords = result.Total,
                TotalNumberOfPages = (int)Math.Ceiling((double)result.Total / pageSize),
                Results = result.Documents,
                ScrollId = result.ScrollId
            };
        }

        public async Task<PagedResult<T>> ScrollAsync<T>(string scrollId) where T : class
        {
            var pageSize = 10;
            var client = await GetClientAsync();
            var searchResponse = await client.ScrollAsync<T>("5m", scrollId);
            if (searchResponse.IsValid)
            {
                return new PagedResult<T>()
                {
                    PageSize = pageSize,
                    TotalNumberOfRecords = searchResponse.Total,
                    TotalNumberOfPages = (int)Math.Ceiling((double)searchResponse.Total / pageSize),
                    Results = searchResponse.Documents,
                    ScrollId = searchResponse.ScrollId
                };
            }
            else
            {
                return null;
            }
        }

        public async Task<List<string>> SearchTagsAsync(string companyNumber, string searchText)
        {
            var boolQuery = new BoolQuery();
            var musts = new List<QueryContainer>();
            musts.Add(new TermQuery()
            {
                Field = new Field("companyNumber.keyword"),
                Value = companyNumber
            });
            musts.Add(new MatchPhrasePrefixQuery()
            {
                Field = new Field("tags"),
                Query = searchText
                
            });
            boolQuery.Must = musts;

            var client = await GetClientAsync();
            return await client.SearchAsync<File>(fl => fl
                                .Size(100)
                                .Index(_storageName)
                                .Type(GetTypeName<File>())
                                .Sort(ss => ss.Descending(SortSpecialField.Score))
                                .Query(q => boolQuery))
                                .ContinueWith(t => {
                                    var x = Encoding.UTF8.GetString(t.Result.ApiCall.RequestBodyInBytes);
                                    return t.Result.Documents.SelectMany(d => d.Tags.Where(v => v.Contains(searchText, StringComparison.OrdinalIgnoreCase))).Distinct().ToList();
                                });
        }

        private string GetTypeName<T>() where T : class
        {
            return typeof(T).Name.ToLowerInvariant();
        }


    }
}
