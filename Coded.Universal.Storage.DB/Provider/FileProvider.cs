﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using File = Coded.Universal.Storage.DB.Model.File;
using System.Linq;
using System.Threading.Tasks;
using Coded.Universal.Storage.DB.Provider.Interface;
using Coded.Universal.Storage.ViewModel;
using MongoDB.Bson;
using Microsoft.Extensions.Configuration;
using Coded.Universal.Storage.DB.Model;
using System.Globalization;

namespace Coded.Universal.Storage.DB.Provider
{
    public class FileProvider: IFileProvider
    {
        private IMongoDatabase _mongoDatabase;
        private IElasticsearchProvider _elasticProvider;
        public IMongoCollection<File> Files
        {
            get
            {
                return _mongoDatabase.GetCollection<File>("Files");
            }
        }

        public FileProvider(IConfiguration configuration, IElasticsearchProvider elasticProvider)
        {
            var client = new MongoClient(configuration["MongoDB:ConnectionString"]);
            _mongoDatabase = client.GetDatabase(configuration["MongoDB:DatabaseName"]);
            _elasticProvider = elasticProvider;
        }

        public async Task<List<string>> GetCategoriesAsync()
        {
            var fields = Builders<File>.Projection.Include(f => f.CompanyNumber);
            var list = await Files.Find(_ => true).Project(fields).ToListAsync();
            return list.Distinct().Cast<string>().ToList();
        }

        public async Task<List<File>> GetAsync()
        {
            return await Files.Find(x => true).ToListAsync();
        }

        public async Task<File> GetAsync(ObjectId id)
        {
            return await Files.Find(x => x.Id == id).FirstOrDefaultAsync();
        }

        public async Task<PagedResult<File>> GetAsync(string category, int pageNumber)
        {
            var pageSize = 10;
            var totalCount = await Files.Find(x => x.CompanyNumber.StartsWith(category)).CountDocumentsAsync();

            var result = new PagedResult<File>()
            {
                PageSize = pageSize,
                TotalNumberOfPages = (int)Math.Ceiling((double)totalCount / pageSize),
                TotalNumberOfRecords = totalCount,
                Results = await Files.Find(x => x.CompanyNumber.StartsWith(category)).Skip((pageNumber - 1) * pageSize).Limit(pageSize).ToListAsync(),
                PageNumber = pageNumber
            };
            return result;
        }

        public async Task<PagedResult<File>> SearchAsync(string companyNumber, ResultFilter filter)
        {
            return await _elasticProvider.SearchAsync(companyNumber, filter);
        }

        public async Task<PagedResult<File>> ScrollAsync(string scrollId)
        {
            return await _elasticProvider.ScrollAsync<File>(scrollId);
        }

        public async Task<File> CreateAsync(File file)
        {
            await Files.InsertOneAsync(file);
            return file;
        }

        public async Task CreateIndexAsync()
        {

            var files = await GetAsync();
            await _elasticProvider.CreateIndexesAsync(files);
        }

        public async Task PutIntoIndexAsync(File file)
        {
            await _elasticProvider.PutAsync(file);
        }

        #region Update methods
        public async Task UpdateUrlAsync(ObjectId id, string fileUrl)
        {
            await Files.UpdateOneAsync<File>(x => x.Id == id, Builders<File>.Update.Set(x => x.Url, fileUrl));
        }

        public async Task UpdateTagsAsync(ObjectId id, List<string> tags)
        {
            await Files.UpdateOneAsync<File>(x => x.Id == id, Builders<File>.Update.Set(x => x.Tags, tags));
            await _elasticProvider.UpdateTagsAsync(id, tags);
        }

        public async Task UpdateDescriptionAsync(ObjectId id, string description)
        {
            await Files.UpdateOneAsync<File>(x => x.Id == id, Builders<File>.Update.Set(x => x.Description, description));
            await _elasticProvider.UpdateDescriptionAsync(id, description);
        }

        public async Task UpdateIssueDateAsync(ObjectId id, DateTime? issueDate)
        {
            await Files.UpdateOneAsync<File>(x => x.Id == id, Builders<File>.Update.Set(x => x.IssueDate, issueDate));
            await _elasticProvider.UpdateIssueDateAsync(id, issueDate);
        }
        #endregion Update methods

        public async Task DeleteAsync(ObjectId id)
        {
            await Files.DeleteOneAsync(Builders<File>.Filter.Eq(x => x.Id, id));
            await _elasticProvider.DeleteFileAsync(id);
        }

        public async Task DeleteAllAsync()
        {
            await Files.DeleteManyAsync(_ => true);
            await _elasticProvider.DeleteAllFilesAsync();
        }

        public async Task ProduceTestDataAsync(string category)
        {
            for (var i = 0; i < 1000; i++)
            {
                var fileName = GetRandomWord() + ".jpg";
                var file = new File()
                {
                    CompanyNumber = category,
                    FileName = fileName,
                    Url = "http://localhost/" + fileName,
                    Description = string.Join(' ',  GetRandomWords(new Random().Next(3) + 1, false))
                };
                file.Tags = GetRandomWords(new Random().Next(3) + 1, true);
                await Files.InsertOneAsync(file);
                await _elasticProvider.PutAsync(file);
            }
        }

        private string GetRandomWord()
        {
            string[] randomWords = new string[] {
                "Cat", "Rocket", "Star", "fusion", "surgery", "greenhouse", "vertical", "foxtrot", "climber", "Fountain", "waterpark", "hedghock", "reptile", "strategy", "impossible",
                "awkward", "singaporean", "vegitarian", "surfing", "bridge", "bird", "hairstyle", "exciting", "hilarious", "standard", "ambitious", "vote", "bleame", "frighten", "barbarian",
                "construction", "room", "stack", "incredible", "concert", "event", "football", "various", "empty", "kickstart", "countdown", "Australia", "ocean", "skyblue", "Sunshine",
                "freedom", "forest", "nationality", "quiz", "at", "value", "attack", "brilliant", "Bill", "beer", "dog", "fighter", "independant", "sentence", "innocent", "Uniform", "collaboration"
            };
            var randomGenerator = new Random();
            var wordsCount = randomGenerator.Next(3) + 1;
            var result = new StringBuilder();
            for (var i = 0; i < wordsCount; i++)
            {
                if (result.Length > 0)
                {
                    result.Append(" ");
                }
                var word = randomWords[randomGenerator.Next(randomWords.Length)];
                while (result.ToString().Contains(word))
                {
                    word = randomWords[randomGenerator.Next(randomWords.Length)];
                }
                result.Append(word);
            }
            return result.ToString();
        }

        private List<string> GetRandomWords(int count, bool toLower)
        {
            var result = new List<string>(count);
            for (var i = 0; i < count; i++)
            {
                var word = GetRandomWord();
                while (result.Contains(word))
                {
                    word = GetRandomWord();
                }
                if (toLower)
                {
                    word = word.ToLowerInvariant();
                }
                result.Add(word);
            }
            return result;
        }
    }
}
